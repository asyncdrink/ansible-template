# Ansible Template

This Ansible template was written as part of the [Async Drink's Ansible articles](https://asyncdrink.com/tags/ansible). You are free to use this template in any way you see fit.

## Ansible roles

As part of this template several basic roles have been included. These are discussed in greater detail in [this article](https://asyncdrink.com/blog/ansible-basic-roles).

## Getting started

### Configure shared group variables

First take a look at `group_vars/all.yml` as you'll need to fill some fields:

`user_name` should be the name of the user you want to use.

`user_password` is the hashed password of the user. To generate a new hashed password do:

```
mkpasswd --method=sha-512
```

`letsencrypt_email` is only relevant if you plan to use the Let's Encrypt role.

### Take a look at the `sshd_config`

I have included a basic `sshd_config` in `config/ssh/sshd_config`. It is mostly the default `sshd_config` with the exception that root logins are disabled over SSH and only public/private key based logins are allowed.

### Adding a new host

Use the `hosts` file in the root of the project to add a new host. Next make sure you have a host variable file in `host_vars` with the exact same name. At a minimum you should define the `ansible_host` variable so Ansible knows how to connect.
